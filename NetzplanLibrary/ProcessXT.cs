﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetzplanLibrary
{
    public class ProcessXT : Process
    {
        protected int gp = -1;
        protected int fgp = -1;

        // Quelle: https://stackoverflow.com/questions/223058/how-to-inherit-constructors
        public ProcessXT(int id, string name, int duration) : base(id, name, duration)
        {

        }

        public void calculateGP()
        {
            this.gp = this.saz - this.faz;
        }

        public void calculateFGP()
        {
            if (this.isEndProcess())
                this.fgp = 0;
            else
                this.fgp = this.succProcesses[0].getFAZ() - this.fez;
        }

        public bool isCritical()
        {
            this.calculateGP();

            return this.gp == 0;
        }
    }
}
