﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;

namespace NetzplanLibrary
{
    public class ProcessXTGui : ProcessXT
    {
        public int posX = 0;
        public int posY = 0;
        public ProcessXTGui(int id, string name, int duration) : base(id, name, duration)
        {

        }

        public List<Process> getPredecessor()
        {
            return this.preProcesses;
        }

        public List<Process> getSuccessor()
        {
            return this.succProcesses;
        }

        public int getFEZ()
        {
            return this.fez;
        }

        public int getSAZ()
        {
            return this.saz;
        }

        public int getSEZ()
        {
            return this.sez;
        }

        public int getPuffer()
        {
            return this.gp;
        }

        public int getFreierPuffer()
        {
            return this.fgp;
        }
    }
}
