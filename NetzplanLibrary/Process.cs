﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetzplanLibrary
{
    public class Process
    {
        private int id;
        private string name;
        private int duration;
        protected int faz = -1;
        protected int saz = -1;

        protected int fez = -1;
        protected int sez = -1;

        protected List<Process> preProcesses = new List<Process>();
        protected List<Process> succProcesses = new List<Process>();

        public Process(int id, string name, int duration)
        {
            this.id = id;
            this.name = name;
            this.duration = duration;
        }

        public int getID()
        {
            return this.id;
        }

        public int getFAZ()
        {
            return this.faz;
        }

        public void setID(int id)
        {
            this.id = id;
        }

        public string getName()
        {
            return this.name;
        }

        public void setName(string name)
        {
            this.name = name;
        }

        public void setDuration(int duration)
        {
            this.duration = duration;
        }

        public void calculateAllStartEndTimes()
        {
            this.calcFAZ();
            this.calcFEZ();
            foreach (Process process in this.succProcesses)
            {
                process.calculateAllStartEndTimes();
            }
        }

        public void calculateAllLatestEndTimes()
        {
            if (this.isEndProcess())
                this.sez = this.fez;
            else if (this.isStartProcess())
            {
                this.sez = 0;
                this.saz = 0;
                return;
            }
            else
            {
                foreach (Process process in this.succProcesses)
                {
                    if (this.sez == -1 || this.sez < process.saz)
                    {
                        this.sez = process.saz;
                    }
                }
            }

            this.saz = this.sez - this.duration;

            bool isCritical = this.isCritical();

            foreach (Process process in this.preProcesses)
            {
                process.calculateAllLatestEndTimes();
            }

        }

        public void addPredecessor(Process preProcess)
        {
            this.preProcesses.Add(preProcess);
        }

        public void addSuccessor(Process succProcess)
        {
            this.succProcesses.Add(succProcess);
        }

        public bool isPredecessor(Process process)
        {
            for (int i = 0; i < this.preProcesses.Count; i++)
            {
                if (this.preProcesses[i].id == process.id)
                {
                    return true;
                }
            }
            return false;
        }

        public void safeAddSuccessor(Process succProcess)
        {
            if (!isPredecessor(succProcess))
            {
                addSuccessor(succProcess);
            }
        }

        public bool isStartProcess()
        {
            if (this.preProcesses.Count == 0)
                return true;
            else
                return false;
        }

        public bool isEndProcess()
        {
            if (this.succProcesses.Count == 0)
                return true;
            else
                return false;
        }

        public void calcFAZ()
        {
            if (isStartProcess())
            {
                this.faz = 0;
                this.fez = this.faz + this.duration;
            }
            else
            {
                for (int i = 0; i < this.preProcesses.Count; i++)
                {
                    if (this.faz == -1 || this.faz < this.preProcesses[i].fez)
                        this.faz = this.preProcesses[i].fez;
                }
            }
        }

        public bool isCritical()
        {
            return this.faz == this.saz;
        }

        public void calcFEZ()
        {
            this.fez = this.faz + this.duration;
        }

        public void calcSEZ()
        {
            if (isEndProcess())
            {
                this.sez = this.saz + this.duration;
            }
            else
            {
                for (int i = 0; i < this.preProcesses.Count; i++)
                {
                    if (this.sez < this.preProcesses[i].sez)
                        this.sez += this.preProcesses[i].sez;
                }
            }
        }

        public int getDauer()
        {
            return this.duration;
        }
    }
}
