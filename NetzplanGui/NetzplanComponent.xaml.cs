﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace NetzplanGui
{
    /// <summary>
    /// Interaktionslogik für NetzplanComponent.xaml
    /// </summary>
    public partial class NetzplanComponent : UserControl
    {
        public object FAZ
        {
            get { return (object)GetValue(FAZProperty); }
            set { SetValue(FAZProperty, value); }
        }

        // Using a DependencyProperty as the backing store for FAZ.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty FAZProperty =
            DependencyProperty.Register("FAZ", typeof(object), typeof(NetzplanComponent), new PropertyMetadata("Test"));



        public NetzplanComponent()
        {
            InitializeComponent();
        }
    }
}
