﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetzplanFormsProject
{
    public class Constants
    {
        public const int width = 150;
        public const int height = 100;
        public const int paddingX = 20;
        public const int paddingY = 20;
    }
}
