﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace Netzplan_Tester
{
    class Program
    {
        // Generelle Quellen:
        // https://projektmanagement24.de/spaetester-anfangszeitpunkt-saz-so-berechnen-sie-den-saz-fuer-den-netzplan-im-projektmanagement-mit-beispiel
        // https://projektmanagement24.de/spaetester-endzeitpunkt-sez-so-berechnen-sie-den-sez-fuer-den-netzplan-im-projektmanagement-mit-beispiel
        static void Main(string[] args)
        {
            NetzplanLibrary.Process start = new NetzplanLibrary.Process(0, "Start", 0);
            NetzplanLibrary.Process a1 = new NetzplanLibrary.Process(1, "A1", 1);
            NetzplanLibrary.Process b1 = new NetzplanLibrary.Process(2, "B1", 1);
            NetzplanLibrary.Process b2 = new NetzplanLibrary.Process(3, "B2", 3);
            NetzplanLibrary.Process end = new NetzplanLibrary.Process(4, "Ende", 0);

            start.safeAddSuccessor(a1);
            a1.addPredecessor(start);
            a1.safeAddSuccessor(b1);
            a1.safeAddSuccessor(b2);
            b1.addPredecessor(a1);
            b1.safeAddSuccessor(end);
            b2.addPredecessor(a1);
            b2.safeAddSuccessor(end);
            end.addPredecessor(b1);
            end.addPredecessor(b2);

            start.calculateAllStartEndTimes();
            end.calculateAllLatestEndTimes();

            // Antwort Aufgabe: i
            bool isCritical = b2.isCritical(); // => B2 isCritical => true

            //zweitesBeispiel();
            pufferBeispiel();
            pufferBeispiel2Internet();
        }

        // Testing Beispiel aus Grafik: https://t2informatik.de/wp-content/uploads/2018/02/netzplan-wissen-kompakt-t2informatik.jpg
        private static void zweitesBeispiel()
        {
            NetzplanLibrary.Process start = new NetzplanLibrary.Process(1, "Start", 0);
            NetzplanLibrary.Process a = new NetzplanLibrary.Process(2, "A", 1);
            NetzplanLibrary.Process b = new NetzplanLibrary.Process(3, "B", 4);
            NetzplanLibrary.Process c = new NetzplanLibrary.Process(4, "C", 5);
            NetzplanLibrary.Process end = new NetzplanLibrary.Process(5, "Ende", 0);

            start.safeAddSuccessor(a);
            start.safeAddSuccessor(b);
            a.safeAddSuccessor(c);
            b.safeAddSuccessor(end);
            c.safeAddSuccessor(end);

            end.addPredecessor(c);
            end.addPredecessor(b);
            c.addPredecessor(a);
            b.addPredecessor(start);
            a.addPredecessor(start);

            start.calculateAllStartEndTimes();
            end.calculateAllLatestEndTimes();

        }

        private static void pufferBeispiel()
        {
            NetzplanLibrary.ProcessXT start = new NetzplanLibrary.ProcessXT(0, "Start", 0);
            NetzplanLibrary.ProcessXT a1 = new NetzplanLibrary.ProcessXT(1, "A1", 1);
            NetzplanLibrary.ProcessXT b1 = new NetzplanLibrary.ProcessXT(2, "B1", 1);
            NetzplanLibrary.ProcessXT b2 = new NetzplanLibrary.ProcessXT(3, "B2", 3);
            NetzplanLibrary.ProcessXT end = new NetzplanLibrary.ProcessXT(4, "Ende", 0);

            start.safeAddSuccessor(a1);
            a1.addPredecessor(start);
            a1.safeAddSuccessor(b1);
            a1.safeAddSuccessor(b2);
            b1.addPredecessor(a1);
            b1.safeAddSuccessor(end);
            b2.addPredecessor(a1);
            b2.safeAddSuccessor(end);
            end.addPredecessor(b1);
            end.addPredecessor(b2);

            start.calculateAllStartEndTimes();
            end.calculateAllLatestEndTimes();

            b1.calculateGP(); // => 2
            b2.calculateGP();
            bool isCriticalNew = b2.isCritical(); // Funktioniert nun über den Gesamtpuffer
        }

        private static void pufferBeispiel2Internet()
        {
            // Beispiel: https://www.fachinformatiker.de/topic/155157-netzplan/
            NetzplanLibrary.ProcessXT a = new NetzplanLibrary.ProcessXT(0, "A", 10);
            NetzplanLibrary.ProcessXT b = new NetzplanLibrary.ProcessXT(1, "B", 8);
            NetzplanLibrary.ProcessXT c = new NetzplanLibrary.ProcessXT(2, "C", 5);
            NetzplanLibrary.ProcessXT d = new NetzplanLibrary.ProcessXT(3, "D", 7);
            NetzplanLibrary.ProcessXT e = new NetzplanLibrary.ProcessXT(4, "E", 5);
            NetzplanLibrary.ProcessXT f = new NetzplanLibrary.ProcessXT(5, "F", 6);
            NetzplanLibrary.ProcessXT g = new NetzplanLibrary.ProcessXT(6, "G", 3);
            NetzplanLibrary.ProcessXT h = new NetzplanLibrary.ProcessXT(7, "H", 2);

            a.safeAddSuccessor(b);
            a.safeAddSuccessor(c);
            a.safeAddSuccessor(d);

            b.safeAddSuccessor(e);
            c.safeAddSuccessor(f);
            d.safeAddSuccessor(f);

            e.safeAddSuccessor(g);
            f.safeAddSuccessor(g);

            g.safeAddSuccessor(h);

            h.addPredecessor(g);

            g.addPredecessor(e);
            g.addPredecessor(f);

            e.addPredecessor(b);

            f.addPredecessor(c);
            f.addPredecessor(d);

            b.addPredecessor(a);
            c.addPredecessor(a);
            d.addPredecessor(a);

            a.calculateAllStartEndTimes();
            h.calculateAllLatestEndTimes();

            a.calculateGP();
            a.calculateFGP();
            b.calculateGP();
            b.calculateFGP();
            c.calculateGP();
            c.calculateFGP();
            d.calculateGP();
            d.calculateFGP();
            e.calculateGP();
            e.calculateFGP();
            f.calculateGP();
            f.calculateFGP();
            g.calculateGP();
            g.calculateFGP();
            h.calculateGP();
            h.calculateFGP();
        }
    }
}
